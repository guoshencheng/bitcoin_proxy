var express = require('express');
var router = express.Router();
var axios = require('axios');
var response = (response ) => {
  if (response.status == 200 && response.data) {
    return response.data;
  } else {
    throw new Error("response not success");
  }
}

var handleData = (key) => (data) => {
  return { [key]: data.last }
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/poloniex', function(req, res, next) {
  axios.get('https://poloniex.com/public?command=returnTicker').then(response => {
    var data = response.data;
    res.json(data);
  }).catch(reason => {
    console.log(reason)
    next(reason);
  })
})

router.get('/quadrigacx', function(req, res, next) {
  var id = req.params.id;
  var eth = axios.get("https://api.quadrigacx.com/v2/ticker?book=eth_cad").then(response).then(handleData("eth"));
  var btc = axios.get("https://api.quadrigacx.com/v2/ticker?book=btc_cad").then(response).then(handleData("btc"));
  Promise.all([eth, btc]).then(values => {
    var cad = values.reduce((pre, cur) => {
      return Object.assign({}, pre, cur);
    }, {})
    if (!cad.eth) delete values.eth;
    if (!cad.btc) delete values.btc;
    res.json(cad);
  }).catch(reason => {
    next()
  });
})

module.exports = router;
